


# Qik Vue UI Kit

[UI Kit Documentation](https://ui.docs.qik.dev) | [SDK Documentation](https://sdk.docs.qik.dev) | [REST API Documentation](https://rest.docs.qik.dev) | [Website](https://qik.dev) 


### Usage
For use in Vue 3 projects where you want to use the Qik UI features and elements. This should be used in conjunction with the Qik Javascript SDK.


### Installation

```bash
npm install @qikdev/vue-ui --save
```

## Getting Started

```js

// Import the Qik sdk
import Qik from '@qikdev/sdk';

//Import the Qik module
import { QikUI, Selection, Device } from '@qikdev/vue-ui';


// Import and create a Vue Application
import { createApp } from 'vue'
import App from './App.vue'
const app = createApp(App)

// Create an instance of the SDK
const sdk = new Qik({
    apiURL: 'production',
});


// Initialise the UI kit
app.use(QikUI, sdk);

// Mount your app
app.mount('#app')

```
