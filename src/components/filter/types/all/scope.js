export default function addScopeFilters(self, fields) {

    // We don't have a glossary of terms
    if (!self.glossary) {
        return;
    }

    const scopeDefinitions = Object.entries(self.glossary)
        .reduce(function(memo, [key, definition]) {

            if (definition.definesType === 'scope') {
                if(definition.key != self.definition.key) {
                    // Don't show options for the same workflow
                    // the user is already looking at
                    memo.push(definition)
                }
            }

            return memo;
        }, [])


    if (!scopeDefinitions.length) {
        return;
    }

    scopeDefinitions.forEach(function(def) {
        fields.push({
            title: `${def.plural}`,
            minimum: 0,
            maximum: 0,
            key: `meta.scopes|${def.key}`,
            type:'reference',
            referenceType:def.key,      
        });

        // ?${def.key}
    })



}