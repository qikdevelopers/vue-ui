export default function addCommentFilters(self, fields) {
  // We don't have a glossary of terms
  if (!self.glossary) {
    return;
  }

  const commentDefinitions = Object.entries(self.glossary).reduce(function (
    memo,
    [key, definition],
  ) {
    if (definition.definesType === "comment" || definition.key === "comment") {
      memo.push(definition);
    }

    // if (definition.definesType === 'comment' || definition.key === 'comment') {
    //     if(definition.key != self.definition.key) {
    //         // Don't show options for the same workflow
    //         // the user is already looking at
    //         memo.push(definition)
    //     }
    // }

    return memo;
  }, []);

  if (!commentDefinitions.length) {
    return;
  }

  const CommentFields = {
    title: ``,
    minimum: 1,
    maximum: 1,
    key: "join_comment",
    asObject: true,
    type: "group",
    fields: [],
  };

  commentDefinitions.forEach(function (def) {
    const cleanedFields = def.fields
      .map(function (field) {
        let output;
        switch (field.key) {
          case "reference":
          case "referenceType":
          case "organisation":
            return false;
            break;
          // case 'due':
          // output = field;
          // break;
          case "meta":
            output = JSON.parse(JSON.stringify(field));
            output.fields = output.fields.filter(function ({ key }) {
              switch (key) {
                case "status":
                case "scopes":
                case "tags":
                case "updated":
                case "created":
                  return true;
                  break;
              }
            });
            break;
        }

        return output;
      })
      .filter(Boolean);

    //////////////////////////////////////////

    const fieldGroupItems = {
      title: ``,
      minimum: 1,
      maximum: 1,
      key: "items[]",
      asObject: true,
      type: "group",
      fields: [
        ...cleanedFields,
        {
          title: ``,
          minimum: 1,
          maximum: 1,
          key: "data",
          asObject: true,
          type: "group",
          fields: def.definedFields,
        },
      ],
    };

    CommentFields.fields.push({
      title: `${def.plural}`,
      minimum: 1,
      maximum: 1,
      key: def.key,
      asObject: true,
      type: "group",
      fields: [
        fieldGroupItems,
        {
          title: "Total",
          minimum: 1,
          maximum: 1,
          key: "total",
          type: "integer",
          referenceType: def.key,
        },
      ],
    });
  });

  fields.push(CommentFields);
}
