import addCheckinFilters from "./profile/checkin";

export default function (self) {
  let allFields = [];

  allFields.push({
    title: `Age`,
    minimum: 1,
    maximum: 1,
    key: "_age",
    type: "integer",
  });

  allFields.push({
    title: `Date of birth`,
    minimum: 1,
    maximum: 1,
    key: "_dob",
    type: "date",
  });

  if (self.glossary) {
    addCheckinFilters(self, allFields);

    const personaDefinition = self.glossary["persona"];

    if (personaDefinition) {
      const cleanedFields = personaDefinition.fields
        .map(function (field) {
          let output = field;
          switch (field.key) {
            case "organisation":
            case "names":
            case "password":
              return;
            case "meta":
              output = JSON.parse(JSON.stringify(field));
              output.fields = output.fields.filter(function ({ key }) {
                switch (key) {
                  case "status":
                  case "scopes":
                  case "tags":
                  case "updated":
                  case "created":
                  case "status":
                    return true;
                    break;
                }
              });
              break;
          }

          return output;
        })
        .filter(Boolean);

      const fieldGroupItems = {
        title: ``,
        minimum: 1,
        maximum: 1,
        key: "items[]",
        asObject: true,
        type: "group",
        fields: cleanedFields,
      };

      const UserPersonaFields = {
        title: `User Login`,
        minimum: 1,
        maximum: 1,
        key: "join_persona",
        asObject: true,
        type: "group",
        fields: [
          fieldGroupItems,
          {
            title: "Total",
            minimum: 1,
            maximum: 1,
            key: "total",
            type: "integer",
            referenceType: "persona",
          },
        ],
      };

      allFields.push(UserPersonaFields);
    }
  }

  return allFields;
}
