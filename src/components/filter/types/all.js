



import addWorkflowFilters from './all/workflow.js';
import addCommentFilters from './all/comment.js';
import addScopeFilters from './all/scope.js';



export default function(self) {

    let fields = [];


    addWorkflowFilters(self, fields);
    addCommentFilters(self, fields);
    addScopeFilters(self, fields);

    return fields;
}