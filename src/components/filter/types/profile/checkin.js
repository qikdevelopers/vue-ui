export default function addCheckinFilters(self, fields) {
  // We don't have a glossary of terms
  if (!self.glossary) {
    return;
  }

  const checkinDefinition = self.glossary["checkin"];

  if (!checkinDefinition) {
    return;
  }

  const CheckinFields = {
    title: `Checkins`,
    minimum: 1,
    maximum: 1,
    key: "join_checkin",
    asObject: true,
    type: "group",
    fields: [
      {
        title: ``,
        minimum: 1,
        maximum: 1,
        key: "checkin",
        asObject: true,
        type: "group",
        fields: [
          {
            title: "Total",
            minimum: 1,
            maximum: 1,
            key: "total",
            type: "integer",
            referenceType: checkinDefinition.key,
          },
        ],
      },
    ],
  };

  fields.push(CheckinFields);
}
