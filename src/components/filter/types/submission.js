export default function(self, definedFields) {


	let allFields = [];

    if (definedFields.length) {

        var formDataFields = {
            title: `Form Data`,
            minimum: 1,
            maximum: 1,
            key: 'formData',
            asObject: true,
            type: 'group',
            fields: definedFields,
        }

        allFields.push(formDataFields);

        const cleanedDataFields = definedFields.map(function(field) {
            if (field.type === 'reference') {
                field = JSON.parse(JSON.stringify(field))
                delete field.fields;
            }

            return field;
        });

        var dataFields = {
            title: `Data`,
            minimum: 1,
            maximum: 1,
            key: 'data',
            asObject: true,
            type: 'group',
            fields: cleanedDataFields,
        }

        allFields.push(dataFields);


    }

    return allFields;
}