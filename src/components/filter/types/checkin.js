export default function (self) {
  let allFields = [];

  allFields.push({
    title: `Event › Start`,
    minimum: 1,
    maximum: 1,
    key: "join_event.startDate",
    type: "date",
    filterIncludeTime: true,
  });

  allFields.push({
    title: `Event › Doors Open`,
    minimum: 1,
    maximum: 1,
    key: "join_event.openDate",
    type: "date",
    filterIncludeTime: true,
  });

  allFields.push({
    title: `Event › Doors Close`,
    minimum: 1,
    maximum: 1,
    key: "join_event.closeDate",
    type: "date",
    filterIncludeTime: true,
  });

  allFields.push({
    title: `Event › End`,
    minimum: 1,
    maximum: 1,
    key: "join_event.endDate",
    type: "date",
    filterIncludeTime: true,
  });

  if (self.glossary) {
    const eventOptions = Object.entries(self.glossary)
      .reduce(function (memo, [key, definition]) {
        if (definition.definesType === "event") {
          memo.push(definition);
        }

        // if (definition.definesType === 'comment' || definition.key === 'comment') {
        //     if(definition.key != self.definition.key) {
        //         // Don't show options for the same workflow
        //         // the user is already looking at
        //         memo.push(definition)
        //     }
        // }

        return memo;
      }, [])
      .map(function ({ title, key }) {
        return {
          title,
          value: key,
        };
      });

    if (eventOptions.length) {
      allFields.push({
        title: `Event › Type`,
        minimum: 1,
        maximum: 1,
        key: "join_event.meta.definition",
        type: "string",
        widget: "select",
        options: eventOptions,
      });
    }
  }

  return allFields;
}
