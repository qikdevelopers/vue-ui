export default {
  props: {
    options: {
      type: Object,
      default() {
        return {};
      },
    },
  },
  data() {
    return {};
  },
  provide() {
    return {
      isInModal: true,
      closeParentModal: (input) => this.close(input),
      dismissParentModal: (input) => this.dismiss(input),
    };
  },
  emits: ["dismiss", "close"],
  methods: {
    dismiss(error) {
      this.$emit("dismiss", error);
    },
    close(result) {
      this.$emit("close", result);
    },
  },
};
