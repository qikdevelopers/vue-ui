import { DateTime } from "luxon";

const service = {};

service.format = function (input, format) {
  input = input ? new Date(input) : new Date();
  const date = DateTime.fromJSDate(input);

  return date.toFormat(format);
};

service.timeago = function (date, options) {
  return DateTime.fromJSDate(new Date(date)).toRelative(options);
};

service.getDatesBetween = function (startDate, endDate) {
  const dates = [];
  let start = DateTime.fromISO(startDate);
  const end = DateTime.fromISO(endDate);

  while (start <= end) {
    dates.push(start.toISODate());
    start = start.plus({ days: 1 });
  }

  return dates;
};

service.getCalendarDateLimits = function (date) {
  const year = date.getFullYear();
  const month = date.getMonth();

  // Get the first day of the month
  const firstDay = new Date(year, month, 1);

  // Get the last day of the month
  const lastDay = new Date(year, month + 1, 0);

  // Calculate the start date (Sunday before or on the first day)
  const startDate = new Date(firstDay);
  startDate.setDate(startDate.getDate() - startDate.getDay());

  // Calculate the end date (Saturday after or on the last day)
  const endDate = new Date(lastDay);
  endDate.setDate(endDate.getDate() + (6 - endDate.getDay()));

  // Format the dates as strings
  const options = {
    weekday: "long",
    day: "numeric",
    month: "long",
    year: "numeric",
  };
  const startDateString = startDate.toLocaleDateString("en-US", options);
  const endDateString = endDate.toLocaleDateString("en-US", options);

  return {
    start: new Date(startDateString),
    end: new Date(endDateString),
  };
};

service.getMiddleDate = function (date1, date2) {
  // Convert the dates to milliseconds
  const ms1 = date1.getTime();
  const ms2 = date2.getTime();

  // Calculate the middle timestamp
  const middleMs = (ms1 + ms2) / 2;

  // Create a new date object from the middle timestamp
  const middleDate = new Date(middleMs);

  return middleDate;
};

service.shortTime = function (datetime) {
  if (datetime.toFormat("mm") === "00") {
    return datetime.toFormat("ha").toLowerCase();
  } else {
    return datetime.toFormat("h:mma").toLowerCase();
  }
};

service.readableEventDate = function ({ startDate, endDate, timezone }) {
  if (!startDate) {
    return "";
  }

  startDate = new Date(startDate);
  endDate = endDate ? new Date(endDate) : undefined;

  // Safely convert JavaScript dates to Luxon DateTime objects
  let start, end;

  try {
    // If timezone is undefined or invalid, it will default to local
    start = timezone
      ? DateTime.fromJSDate(startDate).setZone(timezone)
      : DateTime.fromJSDate(startDate);

    end = endDate
      ? timezone
        ? DateTime.fromJSDate(endDate).setZone(timezone)
        : DateTime.fromJSDate(endDate)
      : start;
  } catch (e) {
    // If timezone conversion fails, fallback to local
    start = DateTime.fromJSDate(startDate);
    end = endDate ? DateTime.fromJSDate(endDate) : start;
  }

  const now = DateTime.now();

  // Only add timezone suffix if it's provided and valid
  const timezoneSuffix =
    timezone && start.zoneName !== DateTime.local().zoneName
      ? ` (${start.zoneName})`
      : "";

  // Helper function to determine if year should be shown
  const shouldShowYear = start.year !== now.year;

  // Format year suffix if needed
  const yearSuffix = shouldShowYear ? ` ${start.year}` : "";

  // Same day events
  if (start.hasSame(end, "day")) {
    // Show only start time with comma
    const shortTime = service.shortTime(start);
    if (shortTime === "12am") {
      return `${start.toFormat("d MMMM")}${yearSuffix}${timezoneSuffix}`;
    } else {
      return `${service.shortTime(start)}, ${start.toFormat("d MMMM")}${yearSuffix}${timezoneSuffix}`;
    }
  }

  // Events spanning multiple days
  if (start.hasSame(end, "month")) {
    // Same month - no spaces around hyphen
    return `${start.toFormat("d")}-${end.toFormat("d MMMM")}${yearSuffix}${timezoneSuffix}`;
  }

  // Different months
  if (start.hasSame(end, "year")) {
    // Same year - spaces around hyphen for cross-month
    return `${start.toFormat("d MMM")} - ${end.toFormat("d MMM")}${yearSuffix}${timezoneSuffix}`;
  }

  // Different years - spaces around hyphen for cross-month
  return `${start.toFormat("d MMM yyyy")} - ${end.toFormat("d MMM yyyy")}${timezoneSuffix}`;
};

service.readableDateRange = function (date1, date2) {
  if (!date1) {
    return;
  }

  if (!date2) {
    return service.readableDate(date1);
  }

  // Create the dates
  date1 = DateTime.fromJSDate(new Date(date1));
  date2 = DateTime.fromJSDate(new Date(date2));

  const date1Year = date1.toFormat("yyyy");
  const date1Month = date1.toFormat("LLL yyyy");
  const date1Week = date1.toFormat("WW yyyy");
  const date1Day = date1.toFormat("d LLL yyyy");

  const date2Year = date2.toFormat("yyyy");
  const date2Month = date2.toFormat("LLL yyyy");
  const date2Week = date2.toFormat("WW yyyy");
  const date2Day = date2.toFormat("d LLL yyyy");

  // If it's the same day
  if (date1Day === date2Day) {
    // 9am - 12pm today
    const shortTime = service.shortTime(date1);
    if (shortTime === "12am") {
      return `${service.readableDate(date2)}`;
    } else {
      return `${service.shortTime(date1)} - ${service.readableDate(date2)}`;
    }
  }

  // If it's the same week
  if (date1Week === date2Week) {
    // 9am tomorrow - 6pm thursday
    return `${service.readableDate(date1)} - ${service.readableDate(date2)}`;
  }

  // If it's the same Month
  if (date1Month === date2Month) {
    // 1 - 22 May
    return `${date1.toFormat("d")} - ${date2.toFormat("d LLL")}`;
  }

  // If it's the same Year
  if (date1Year === date2Year) {
    // Monday 12 June - Thursday 12 July
    return `${service.readableDate(date1)} - ${service.readableDate(date2)}`;
  }

  return `${service.readableDate(date1)} - ${service.readableDate(date2)}`;
};

service.readableDate = function (date) {
  if (!date) {
    return;
  }

  const now = new Date();
  const nowDate = DateTime.fromJSDate(now);
  const currentYear = nowDate.toFormat("yyyy");
  const currentMonth = nowDate.toFormat("LLL yyyy");
  const currentWeek = nowDate.toFormat("WW yyyy");
  const currentDay = nowDate.toFormat("d LLL yyyy");

  // Get the pieces of the date we are checking
  date = new Date(date);
  const checkDate = DateTime.fromJSDate(date);
  const checkYear = checkDate.toFormat("yyyy");
  const checkMonth = checkDate.toFormat("LLL yyyy");
  const checkWeek = checkDate.toFormat("WW yyyy");
  const checkDay = checkDate.toFormat("d LLL yyyy");

  // If it's the same day
  if (checkDay === currentDay) {
    return `${service.shortTime(checkDate)} today`;
  }

  // If it's the same week
  if (checkWeek === currentWeek) {
    if (date > now) {
      const dateDayInt = parseInt(nowDate.toFormat("d"));
      const checkDayInt = parseInt(checkDate.toFormat("d"));

      if (checkDayInt - dateDayInt === 1) {
        return `${service.shortTime(checkDate)} tomorrow`;
      }

      return `${service.shortTime(checkDate)} ${checkDate.toFormat("cccc")}`;
    } else {
      // In the past this week
      return `${checkDate.toRelativeCalendar()}`;
    }
  }

  // If it's the same month
  if (checkMonth === currentMonth) {
    const shortTime = service.shortTime(checkDate);
    if (shortTime === "12am") {
      return `${checkDate.toFormat("d LLL")}`;
    } else {
      return `${service.shortTime(checkDate)} ${checkDate.toFormat("d LLL")}`;
    }
  }

  if (checkYear === currentYear) {
    return checkDate.toFormat("cccc d LLL");
  } else {
    return checkDate.toFormat("d LLL yyyy");
  }
};

export default service;
